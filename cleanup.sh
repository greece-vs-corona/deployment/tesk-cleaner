#!/bin/bash

set -e

namespace=${NAMESPACE:-'default'}

cleanup_threshold=${CLEANUP_THRESHOLD:-'100'}

#use coreutils date instead of the alpine variant for better compatibility with hosts
now_seconds=$(coreutils --coreutils-prog=date +%s)

completed_jobs=$(kubectl get jobs -n $namespace -o jsonpath='{.items[?(@.status.completionTime)].metadata.name}')

if [ -z "$completed_jobs" ]
then
    echo "no completed jobs found"
else
    echo "found completed jobs: $completed_jobs"
fi

for job in $completed_jobs
do
  if [[ $job == *"-outputs-filer" ]]; then

    completed_at=$(kubectl get job $job -n $namespace -o jsonpath='{.status.completionTime}')

    started_seconds=$(coreutils --coreutils-prog=date -d $completed_at +"%s")

    completed_for_seconds=$(($now_seconds-$started_seconds))

    echo "job $job is in completed state for $completed_for_seconds seconds"

    if [ "$completed_for_seconds" -gt "$cleanup_threshold" ]
    then

        task_id=${job%-outputs-filer}

        echo "find pods from task $task_id and print and delete them if any"

        kubectl get pods --no-headers -o custom-columns=":metadata.name" | grep ^$task_id | xargs --no-run-if-empty kubectl delete pod

    fi
  fi
done
